let num = prompt("Enter number: ");

console.log("The number you provided is " + num);

for(let i = num; i <= num; i--){
    if(i <= 50){
        console.log("The current value is at " + i + " Terminating the loop.");
        break;
    } else if(i % 10 === 0){
        console.log("The number is divisible by 10. Skipping the number.");
        continue;
    } else if(i % 5 === 0){
        console.log(i);
    }
}

let myString = "supercalifragilisticexpialidocious";
let myNewString = "";

for(let i = 0; i < myString.length; i++){
    if(myString[i] === "a" || myString[i] === "e" || myString[i] === "i" || myString[i] === "o" || myString[i] === "u"){
        continue;
    } else {
        myNewString += myString[i];
    }
}
console.log(myString);
console.log(myNewString);